Accumulo Groovy Library
=======================
This library is written to make using accumulo more groovy like.  Makes things like scanning look like a collection
that you configure using Groovy's BuilderSupport system. It wraps the "connector" object into a "AcculumoConnecion"
object which will allow you to create scanners, batchscanner, and batchwriters from it.

Examples
--------
### Create an instance of AccumuloConnection
These params will be given to create an Accumulo Connector object with creating a ZooKeeperInstance with the params provided
The tablePrefix will force any table operations to use the prefix given for the table name, thus generating a "namespace" for
the tables.  For MockConnections, you can use the parameter: "useMock:"
If you want the connection object to auto create tables before scanning or writing, you can set the createTablesIfNeeded option
to true within the params.

        def params = [
                instanceName: "instance",
                zookeeper: ["a", "b", "c"],
                username: "test",
                password: "password",
                tablePrefix: "prefix"
        ]
        AccumuloConnection connection = new AccumuloConnection(params)

### Create a table if it does not already exist

        connection.createTableIfNeeded("testScanner")

### Scan over a table
This will scan over the table, "prefix_myTable" with the Range of row ids being: ["a","b"]
with a columnFamily of "MyCF" and authorizations set to ["a", "b", "c"], and print the rowId=value
Other collection-esk functions are available like; "collect()", "sortBy", "countBy", "groupBy", "sum","min","max","join","find"

        connection.withScanner(tableName: "myTable", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
            fetchColumn(columnFamily:"MyCF")
        }.each { Key k, Value v ->
            println k.row.toString() + "=" + v.toString()
        }

### BatchScan over a table
This will look for all items in the collection "searchItems" as the rowIds, and get the values for the column "MyCF:MyQual"
This will perform the query, get the results, close the batchScanner afterwards, then join all of the value's into a comma
separated string.

      connection.withBatchScanner(tableName: "myTable", authorizations: ["a", "b", "c"]) {
            searchItems.each { item ->
                range(item)
            }
            fetchColumn(columnFamily:"MyCF", columnQualifier:"MyQual")
        }.join(',') { result -> result.value  }


### Write rows using batchWriter
This is probably pretty self explanatory, but it will create 6 rows + number of items in the "toSave" collection.  It will
then automatically close the writer.  You can set more options on the writer by passing it in the call arguments:
Each addMutation block can take in defaults for the "put" lines that follow. Also the withWriter function takes in these
optional arguments: maxMemory, maxLatency, and writeThreads.

        connection.withWriter("testBatchWriter") {
            addMutation("rowId") {
                put(columnFamily: "cf", columnQualifier: "cq", columnVisibility: null, value: "Some value here")
                put(columnFamily: "cf2", columnQualifier: "cq1", columnVisibility: null, value: "WOW!")
            }
            put(rowId: "anotherRowId", columnFamily: "cf2", columnQualifier: "cq1", columnVisibility: "a&b", value: "How now!")
            addMutation("yetAnotherRowId", columnVisibility:"(a&b)|(y&z)") {
                put(columnFamily: "cf", columnQualifier: "cq", value: "Awesome values go in here")
                put(columnFamily: "cf2", columnQualifier: "cq1", value: "She couldn't believe her luck as she discovered him shaking hands with the turtle")
            }
            put(rowId: "rowIdFool", columnFamily: "cfa", columnQualifier: "cqa", columnVisibility: "a", value: "Lucky")
            addMutation(columnFamily:"cf", columnQualifier:"cq") {
                toSave.each { item ->
                    put(columnVisibility:item.visibility, value:item.value)
                }
            }
        }

### Scan with an iterator set
You can call properties of iterators just by using the '=', and then call functions on the iterator within the block
to configure the iterator.

      connection.withBatchScanner(tableName: "myTable", authorizations: ["a", "b", "c"]) {
            searchItems.each { item ->
                range(item)
            }
            scanIterator(priority: 15, iteratorClass: TimestampFilter.class) {
                start = 1359846899
                startInclusive()
                end = 1359847100
            }
            fetchColumn(columnFamily:"MyCF", columnQualifier:"MyQual")
        }.join(',') { result -> result.value  }

