/*
   Copyright 2013 James Stapleton

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package com.tasermonkeys.groovy.accumulo

import com.tasermonkeys.groovy.accumulo.internal.BatchScanner_T
import com.tasermonkeys.groovy.accumulo.internal.Scanner_T
import groovy.util.logging.Log4j
import org.apache.accumulo.core.client.*
import org.apache.accumulo.core.client.admin.TimeType
import org.apache.accumulo.core.client.mock.MockInstance
import org.apache.accumulo.core.security.Authorizations
import org.apache.accumulo.core.security.thrift.AuthInfo

import static com.tasermonkeys.groovy.accumulo.AccumuloUtils.asAuths

@Log4j
class AccumuloConnection {
    private static final Long DEFAULT_MAX_WRITE_MEMORY = 50 * 1024 * 1024l;
    private static final Long DEFAULT_MAX_WRITE_LATENCY = 2 * 60 * 1000l;
    private static final Integer DEFAULT_MAX_WRITE_THREADS = 3;
    private static final Integer DEFAULT_QUERY_THREADS = 10;
    private static final Boolean DEFAULT_CREATE_TABLE_IF_NEEDED = false;

    def final Connector connector
    def final String tablePrefix
    def final queryThreads
    def final maxWriteMemory
    def final maxWriteLatency
    def final writeThreads
    def final shouldCreateTableIfNeeded

    def AccumuloConnection(Map params) {
        if (!params.containsKey("zookeeper") ||
                !(params.containsKey("instanceName") || params.containsKey("instanceUUID")) ||
                !((params.containsKey("username") &&
                        params.containsKey("password")) ||
                        params.containsKey("authInfo"))
        ) {
            throw new IllegalArgumentException("Expect all of the following parameters: zookeeper, instanceName, username, password")
        }
        def Instance instance
        String instanceName = params['instanceName'] as String
        if (params['useMock']) {
            log.info "Using mock connection on instanceName \"${instanceName}\""
            instance = new MockInstance(instanceName)
        } else {
            def int sessionTimeout = params['sessionTimeout'] as int
            def String zookeeperNodes = nodeListToString(params['zookeeper'])
            def instanceUUID = parseInstanceUUID(params['instanceUUID'])
            if (sessionTimeout != null && instanceName != null)
                instance = new ZooKeeperInstance(instanceName, zookeeperNodes, sessionTimeout)
            else if (sessionTimeout == null && instanceName != null)
                instance = new ZooKeeperInstance(instanceName, zookeeperNodes)
            else if (sessionTimeout != null && instanceUUID != null)
                instance = new ZooKeeperInstance(instanceUUID, zookeeperNodes, sessionTimeout)
            else if (sessionTimeout == null && instanceUUID != null)
                instance = new ZooKeeperInstance(instanceUUID, zookeeperNodes)
            else
                throw new IllegalArgumentException("Can not create instance with combination of arguments")
            log.info "Created connection on instanceName ${instanceName ?: instanceUUID}" + (sessionTimeout != null ? " with sessionTimeout of ${sessionTimeout}" : "")
        }
        if (params['authInfo']) {
            connector = instance.getConnector((AuthInfo) params['authInfo'])
        } else {
            connector = instance.getConnector(params['username'] as String, params['password'] as String)
        }
        tablePrefix = params['tablePrefix']
        if (tablePrefix != null && !tablePrefix.endsWith("_")) {
            tablePrefix += "_"
        } else if (tablePrefix == null)
            tablePrefix = ""

        queryThreads = params['queryThreads'] as Integer ?: DEFAULT_QUERY_THREADS
        maxWriteMemory = params['maxWriteMemory'] as Integer ?: DEFAULT_MAX_WRITE_MEMORY
        maxWriteLatency = params['maxWriteLatency'] as Integer ?: DEFAULT_MAX_WRITE_LATENCY
        writeThreads = params['writeThreads'] as Integer ?: DEFAULT_MAX_WRITE_THREADS
        shouldCreateTableIfNeeded = getDefaultShouldCreateTable(params)

    }

    def createWriter(String tableName) {
        if (parseShouldCreateTableIfNeeded(null))
            createTableIfNeeded()
        connector.createBatchWriter(getTableName(tableName), parseMaxWriteMemory(null),
                parseMaxLatency(null), parseWriteThreads(null))
    }

    def createWriter(Map params) {
        if (parseShouldCreateTableIfNeeded(params["createTablesIfNeeded"]))
            createTableIfNeeded(params)
        connector.createBatchWriter(getTableName(params['tableName'].toString()),
                parseMaxWriteMemory(params['maxMemory']),
                parseMaxLatency(params['maxLatency']),
                parseWriteThreads(params['writeThreads']))

    }

    def withScanner(Map params, Closure builderClosure, Closure scannerClosure) {
        def sb = new ScannerBuilder(this)
        sb(params, builderClosure).scan(scannerClosure)
    }

    def Scanner_T withScanner(Map params, Closure builderClosure) {
        def sb = new ScannerBuilder(this)
        sb(params, builderClosure)
    }

    def withBatchScanner(Map params, Closure builderClosure, Closure scannerClosure) {
        def bsb = new BatchScannerBuilder(this)
        //noinspection GroovyAssignabilityCheck
        bsb(params, builderClosure).scan(scannerClosure)
    }

    @SuppressWarnings("GroovyUnusedDeclaration")
    def BatchScanner_T withBatchScanner(Map params, Closure builderClosure) {
        def bsb = new BatchScannerBuilder(this)
        //noinspection GroovyAssignabilityCheck
        bsb(params, builderClosure)
    }

    def withWriter(String tableName, closure) {
        def bwb = new BatchWriterBuilder(this, [tableName: tableName])
        bwb(closure)
    }

    def withWriter(Map params, closure) {
        new BatchWriterBuilder(this, params)(closure)
    }

    def Scanner createScanner(Map map) {
        createScanner(map['tableName'].toString(), map['authorizations'])
    }

    def Scanner createScanner(String tableName, List auths) {
        tableName = getTableName(tableName)
        Authorizations authorizations = asAuths(auths)
        makeSingleScannerWithOptions(tableName, authorizations)
    }

    def BatchScanner createBatchScanner(Map map) {
        String tableName = getTableName(map['tableName'].toString())
        Authorizations auths = asAuths(map['authorizations'])
        int scanQueryThreads = parseQueryThreads(map['queryThreads'])
        makeScannerWithOptions(tableName, auths, scanQueryThreads)
    }

    // This is to allow a mock instance to override this
    protected BatchScanner makeScannerWithOptions(String tableName, Authorizations auths, int scanQueryThreads) {
        connector.createBatchScanner(tableName, auths, scanQueryThreads)
    }

    // This is to allow a mock instance to override this
    protected Scanner makeSingleScannerWithOptions(String tableName, Authorizations auths) {
        connector.createScanner(tableName, auths)
    }


    private static UUID parseInstanceUUID(uuid) {
        if (uuid == null) return null
        uuid instanceof String ? UUID.fromString(uuid) : uuid
    }

    private static String nodeListToString(lst) {
        lst instanceof List ? lst.join(',') : lst
    }

    private def parseShouldCreateTableIfNeeded(val) {
        parseBooleanWithDefValue(val, shouldCreateTableIfNeeded)
    }

    private def parseQueryThreads(val) {
        parseIntWithDefValue(val, queryThreads)
    }

    private def parseMaxWriteMemory(val) {
        parseIntWithDefValue(val, maxWriteMemory)
    }

    private def parseMaxLatency(val) {
        parseIntWithDefValue(val, maxWriteLatency)
    }

    private def parseWriteThreads(val) {
        parseIntWithDefValue(val, writeThreads)
    }

    private static def parseIntWithDefValue(val, defValue) {
        if (val == null) return defValue
        if (val instanceof Number) return val.intValue()
        val.toString().toInteger()
    }

    private static def parseBooleanWithDefValue(val, defValue) {
        if (val == null) return defValue
        if (val instanceof Boolean) return val
        val.toString().toBoolean()
    }

    public def createTableIfNeeded(tableName) {
        if (tableName instanceof Map) tableName = tableName["tableName"]
        tableName = getTableName(tableName.toString())
        if (!connector.tableOperations().exists(tableName)) {
            connector.tableOperations().create(tableName)
        }
    }

    public def createTableIfNeeded(tableName, boolean limitVersion) {
        if (tableName instanceof Map) tableName = tableName["tableName"]
        tableName = getTableName(tableName.toString())
        if (!connector.tableOperations().exists(tableName)) {
            connector.tableOperations().create(tableName, limitVersion)
        }
    }

    public def createTableIfNeeded(tableName, boolean limitVersion, TimeType timeType) {
        tableName = getTableName(tableName.toString())
        if (!connector.tableOperations().exists(tableName)) {
            connector.tableOperations().create(tableName, limitVersion, timeType)
        }
    }

    def getTableName(String tableName) {
        if (tableName == null) throw new IllegalArgumentException("TableName cannot be null!")
        return tablePrefix + tableName
    }

    private static boolean getDefaultShouldCreateTable(Map params) {
        // if createTableIfNeeded is specified use that value
        // otherwise, if useMock is specified use that value
        // otherwise, DEFAULT_CREATE_TABLE_IF_NEEDED
        if (params.containsKey('createTablesIfNeeded'))
            params['createTablesIfNeeded'].toString().toBoolean()
        else if (params.containsKey("useMock"))
            params['useMock'].toString().toBoolean()
        else
            DEFAULT_CREATE_TABLE_IF_NEEDED
    }
}
