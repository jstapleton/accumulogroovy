/*
   Copyright 2013 James Stapleton

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package com.tasermonkeys.groovy.accumulo

import groovy.util.logging.Log4j
import org.apache.accumulo.core.data.Key
import org.apache.accumulo.core.data.Range
import org.apache.accumulo.core.data.Value
import org.apache.accumulo.core.security.Authorizations
import org.apache.accumulo.core.security.ColumnVisibility
import org.apache.accumulo.core.util.TextUtil
import org.apache.hadoop.io.Text

import java.util.concurrent.atomic.AtomicBoolean

@Log4j
class AccumuloUtils {
    public static final AtomicBoolean CLOSE_VERBOSELY = new AtomicBoolean(false)

    /**
     * If the object has a close method call it.  Otherwise do not call it and do nothing.
     * @param resource the resource to close(if responds to close) without Exceptions propagating up
     */
    public static void closeQuietly(def resource) {
        try {
            //noinspection GrUnresolvedAccess
            if (resource.respondsTo("close")) resource.close()
        } catch (Exception e) {
            if (CLOSE_VERBOSELY.get()) {
                log.error "Error closing resource of type: ${resource.getClass().name}", e
            }
        }
    }

    private static final String[] EMPTY_STRING_ARRAY = new String[0];

    public static Authorizations asAuths(auths) {
        if (auths == null) return null
        if (auths instanceof Authorizations) return auths
        if (auths instanceof List) {
            String[] ar = auths.toArray(EMPTY_STRING_ARRAY)
            return new Authorizations(ar)
        } else if (auths instanceof String) {
            return new Authorizations(auths as String)
        } else {
            throw new IllegalArgumentException("Invalid type to convert to auths")
        }

    }

    public static Key asKey(key) {
        if (key == null) return null
        if (key instanceof Key) return key
        if (key instanceof Map) {
            Text row = asText(key['rowId'])
            Text cf = asText(key['columnFamily'])
            Text cq = asText(key['columnQualifier'])
            ColumnVisibility cv = asVisibility(key['columnVisibility'])
            Integer ts = asTimeStamp(key['timestamp'])
            if (!row) throw new IllegalArgumentException("A key must have a row id")
            if (cf && cq && cv && ts) {
                return new Key(row, cf, cq, cv, ts)
            } else if (cf && cq && cv && !ts) {
                return new Key(row, cf, cq, cv, Long.MAX_VALUE)
            } else if (cf && cq && !cv && ts) {
                return new Key(row, cf, cq, ts)
            } else if (cf && cq && !cv && !ts) {
                return new Key(row, cf, cq)
            } else if (cf && !cq && !cv && !ts) {
                return new Key(row, cf)
            } else if (!cf && !cq && !cv && !ts) {
                return new Key(row)
            }
            throw new IllegalArgumentException("Invalid combination of attributes to form a key")
        } else if (key instanceof String) {
            return new Key(key)
        } else if (key instanceof Text) {
            return new Key(key)
        }
        throw new IllegalArgumentException("Can not convert from class ${key.class.name} to a Key")
    }

    public static Text asText(text) {
        if (text == null) return null
        if (text instanceof Text) return text
        if (text instanceof String || text instanceof char) return new Text(text as String)
        if (text instanceof byte[]) return new Text(text as byte[])
        if (text instanceof Long) {
            return new Text(String.format("%019d", text as long))
        }
        if (text instanceof Integer) {
            return new Text(String.format("%010d", text as int))
        }
        if (text instanceof Short) {
            return new Text(String.format("%05d", text as short))
        }
        if (text instanceof byte) {
            return new Text(String.format("%03d", text as byte))
        }
        if (text instanceof boolean) {
            return new Text(text ? "1" : "0")
        }
        if (text instanceof Date) {
            //noinspection SpellCheckingInspection
            return new Text(text.format("yyyyMMddHHmmss"))
        }
        if (text instanceof Calendar) {
            //noinspection SpellCheckingInspection
            return new Text(text.format("yyyyMMddHHmmss"))
        }
        return new Text(text.toString())
    }

    public static Long asTimeStamp(ts) {
        if (ts == null) return null
        if (ts instanceof Integer) return ts
        if (ts instanceof Long) return ts
        if (ts instanceof Date) return ts.time
        if (ts instanceof Calendar) return ts.time.time
        return ts.toString().toLong()
    }

    public static ColumnVisibility asVisibility(vis) {
        if (vis == null) return null
        if (vis instanceof ColumnVisibility) return vis
        if (vis instanceof String) return new ColumnVisibility(vis as String)
        if (vis instanceof byte[]) return new ColumnVisibility(vis as byte[])
        throw new IllegalArgumentException("Can not convert class of type ${vis.getClass().name} to ColumnVisibility")
    }

    static Value asMutationValue(value) {
        if (value instanceof Value) return value
        if (value instanceof Text) return new Value(TextUtil.getBytes(value))
        if (value instanceof byte[]) return new Value(value)
        // man object creation might blow up here!!
        return new Value(TextUtil.getBytes(new Text(value.toString())))
    }

    static Range asRange(Map map) {
        Key startKey = asKey(map['start'])
        Key stopKey = asKey(map['stop'])

        boolean startRowInclusive = map['startRowInclusive'] != null ? map['startRowInclusive'] as boolean : true
        boolean endRowInclusive = map['endRowInclusive'] != null ? map['endRowInclusive'] as boolean : true
        return new Range(startKey, startRowInclusive, stopKey, endRowInclusive)
    }
}
