/*
   Copyright 2013 James Stapleton

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package com.tasermonkeys.groovy.accumulo

import com.tasermonkeys.groovy.accumulo.internal.BaseScannerBuilder
import com.tasermonkeys.groovy.accumulo.internal.BatchScanner_T
import com.tasermonkeys.groovy.accumulo.internal.Ranges_T
import groovy.util.logging.Log4j

@Log4j
class BatchScannerBuilder extends BaseScannerBuilder {

    BatchScannerBuilder(AccumuloConnection connection) {
        super(connection)
    }

    @Override
    protected Object createNode(Object name) {
        if (name in BatchScanner_T.names) {
            return new BatchScanner_T(connection, null)
        } else if (name == Ranges_T.name) {
            return new Ranges_T(null)
        } else {
            return super.createNode(name)
        }
    }

    @Override
    protected Object createNode(Object name, Map attributes) {
        if (name in BatchScanner_T.names) {
            return new BatchScanner_T(connection, attributes)
        } else if (name == Ranges_T.name) {
            return new Ranges_T(attributes)
        } else {
            return super.createNode(name, attributes)
        }
    }

    @Override
    protected void nodeCompleted(Object parent, Object node) {
        if (node instanceof Ranges_T) {
            if (!(parent instanceof BatchScanner_T))
                throw new IllegalArgumentException("Ranges must be a child of the root")
            else
                parent.addItem(node)
        } else {
            super.nodeCompleted(parent, node)
        }
    }
}
