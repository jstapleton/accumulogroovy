/*
 *  Copyright 2013 James Stapleton
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.tasermonkeys.groovy.accumulo

import groovy.util.logging.Log4j
import org.apache.accumulo.core.client.BatchWriter

@Log4j
class BatchWriterBuilder extends BuilderSupport {
    private def AccumuloConnection connection
    private def Map params
    private def BatchWriter batchWriter


    public BatchWriterBuilder(AccumuloConnection connection, Map params) {
        this.connection = connection
        this.params = params
        batchWriter = connection.createWriter(params)
    }



    @Override
    protected void setParent(Object parent, Object child) {
    }

    @Override
    protected Object createNode(Object name) {
        if (name in MutationBuilder.names) {
            return new MutationBuilder()
        }
        return this
    }

    @Override
    protected Object createNode(Object name, Object value) {
        if (name in MutationBuilder.names) {
            return new MutationBuilder(value as String)
        }

        return this
    }

    @Override
    protected Object createNode(Object name, Map attributes) {
        if (name in MutationBuilder.names) {
            return new MutationBuilder(attributes)
        } else if (name in ['put', 'putDelete', 'add', 'insert', 'delete']) {
            if (name in ['add', 'insert']) name = 'put'
            else if (name in ['putDelete']) name = 'delete'
            return new MutationBuilder(attributes)."${name}"(attributes)
        }
        return this
    }

    @Override
    protected Object createNode(Object name, Map attributes, Object value) {
        return this
    }

    @Override
    protected void nodeCompleted(Object parent, Object node) {
        if (node instanceof MutationBuilder) {
            batchWriter.addMutation(node.mutation)
            return
        }
        super.nodeCompleted(parent, node)
    }

    @Override
    protected void setClosureDelegate(Closure closure, Object node) {
        if (node instanceof BuilderSupport) {
            closure.setDelegate(node);
            closure.resolveStrategy = Closure.DELEGATE_ONLY
        } else
            super.setClosureDelegate(closure, node)
    }
}
