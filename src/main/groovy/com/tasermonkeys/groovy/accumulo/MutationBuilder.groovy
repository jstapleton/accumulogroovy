/*
 *  Copyright 2013 James Stapleton
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.tasermonkeys.groovy.accumulo

import groovy.util.logging.Log4j
import org.apache.accumulo.core.data.Mutation
import org.apache.accumulo.core.data.Value
import org.apache.accumulo.core.security.ColumnVisibility
import org.apache.hadoop.io.Text

@Log4j
class MutationBuilder extends BuilderSupport {
    public static List<String> names = ["addMutation", "mutation"]
    Mutation mutation

    MutationBuilder() {
        throw new IllegalArgumentException("Require a rowId to be specified!")
    }

    MutationBuilder(Map params) {
        this(params.get("rowId").toString())
    }

    MutationBuilder(String rowId) {
        this(new Text(rowId))

    }

    MutationBuilder(Text rowId) {
        if (rowId == null) throw new IllegalArgumentException("Require a rowId to be specified!")
        mutation = new Mutation(rowId)
    }

    @Override
    protected void setParent(Object parent, Object child) {

    }

    @Override
    protected Object createNode(Object name) {
        return this
    }

    @Override
    protected Object createNode(Object name, Object value) {
        return this
    }

    @Override
    protected Object createNode(Object name, Map attributes) {
        Text cf = AccumuloUtils.asText(attributes.get("columnFamily"))
        Text cq = AccumuloUtils.asText(attributes.get("columnQualifier"))
        ColumnVisibility cv = AccumuloUtils.asVisibility(attributes.get("columnVisibility"))
        Long ts = AccumuloUtils.asTimeStamp(attributes.get("timestamp"))
        Value value = AccumuloUtils.asMutationValue(attributes.get("value"))
        if (name == "put") {
            put(cf, cq, cv, ts, value)
        } else if (name == "delete") {
            delete(cf, cq, cv, ts)
        } else {
            throw new IllegalArgumentException("Unexpected command ${name.toString()} to MutationBuilder")
        }
        return this

    }

    void put(Text cf, Text cq, ColumnVisibility cv, Long ts, Value value) {
        if (cf && cq && cv && ts && value) {
            mutation.put(cf, cq, cv, ts, value)
        } else if (cf && cq && cv && !ts && value) {
            mutation.put(cf, cq, cv, value)
        } else if (cf && cq && !cv && ts && value) {
            mutation.put(cf, cq, ts, value)
        } else if (cf && cq && !cv && !ts && value) {
            mutation.put(cf, cq, value)
        } else {
            throw new IllegalArgumentException("Invalid combination of arguments to put into a Mutation")
        }
    }

    void delete(Text cf, Text cq, ColumnVisibility cv, long ts) {
        if (cf && cq && cv && ts) {
            mutation.putDelete(cf, cq, cv, ts)
        } else if (cf && cq && cv && !ts) {
            mutation.putDelete(cf, cq, cv)
        } else if (cf && cq && !cv && ts) {
            mutation.putDelete(cf, cq, ts)
        } else if (cf && cq && !cv && !ts) {
            mutation.putDelete(cf, cq)
        } else {
            throw new IllegalArgumentException("Invalid combination of arguments to put into a Mutation")
        }
    }


    @Override
    protected Object createNode(Object name, Map attributes, Object value) {
        return this
    }

    @Override
    protected void setClosureDelegate(Closure closure, Object node) {
        if (node instanceof BuilderSupport) {
            closure.setDelegate(node);
            closure.resolveStrategy = Closure.DELEGATE_ONLY
        } else
            super.setClosureDelegate(closure, node)
    }
}
