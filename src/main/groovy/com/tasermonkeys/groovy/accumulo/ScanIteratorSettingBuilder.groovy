/*
 *  Copyright 2013 James Stapleton
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.tasermonkeys.groovy.accumulo

import org.apache.accumulo.core.client.IteratorSetting
import org.apache.accumulo.core.data.Key
import org.apache.accumulo.core.data.Value
import org.apache.accumulo.core.iterators.SortedKeyValueIterator

class ScanIteratorSettingBuilder extends BuilderSupport {
    static final String name = "scanIterator"
    static final def names = ["scanIterator", "iteratorSetting", "iterator"]
    IteratorSetting is

    private static def int validPriority(priority) {
        if (priority == null) throw new IllegalArgumentException("Priority must be specified for ScanIteratorSetting")
        if (priority instanceof String) {
            return priority.toInteger()
        }
        return priority as int
    }

    ScanIteratorSettingBuilder(Map params) {
        def priority = params['priority']
        def name = params['name']?.toString()
        def iteratorClass = params['iteratorClass']
        // filter out the keys we sucked out
        params = params.findAll { !(it.key in ['priority', 'name', 'iteratorClass']) }.collectEntries { [it.key, it.value.toString()] }
        if (iteratorClass instanceof Class) {
            if (name != null) {
                is = new IteratorSetting(validPriority(priority) as int, name, iteratorClass as Class)
                is.addOptions(params)
            } else {
                //noinspection GroovyAssignabilityCheck
                is = new IteratorSetting(validPriority(priority) as int, iteratorClass as Class, params)
            }
        } else if (iteratorClass instanceof String) {
            if (name == null) throw new IllegalArgumentException("Name must be specified if iteratorClass is a string")
            is = new IteratorSetting(validPriority(priority) as int, name, iteratorClass as String, params)
        } else {
            throw new IllegalArgumentException("iteratorClass must be a string(full class path) or a Class Object")
        }
    }

    ScanIteratorSettingBuilder(int priority, String name, Class<? extends SortedKeyValueIterator<Key, Value>> iteratorClass) {
        is = new IteratorSetting(priority, name, iteratorClass)
    }

    ScanIteratorSettingBuilder(int priority, Class<? extends SortedKeyValueIterator<Key, Value>> iteratorClass) {
        is = new IteratorSetting(priority, iteratorClass)
    }

    ScanIteratorSettingBuilder(int priority, String name, String iteratorClassName) {
        is = new IteratorSetting(priority, name, iteratorClassName)
    }

    // groovy put implicitly the named arguments to the first argument as a Map, so we will suck those up as properties
    ScanIteratorSettingBuilder(Map properties, int priority, String name, String iteratorClass) {
        is = new IteratorSetting(priority, name, iteratorClass, properties.collectEntries { [it.key, it.value.toString()] })
    }
    // groovy put implicitly the named arguments to the first argument as a Map, so we will suck those up as properties
    ScanIteratorSettingBuilder(Map properties, int priority, Class<? extends SortedKeyValueIterator<Key, Value>> iteratorClass) {
        //noinspection GroovyAssignabilityCheck
        is = new IteratorSetting(priority, iteratorClass, properties.collectEntries { [it.key, it.value.toString()] })
    }

    // groovy put implicitly the named arguments to the first argument as a Map, so we will suck those up as properties
    ScanIteratorSettingBuilder(Map properties, int priority, String name, Class<? extends SortedKeyValueIterator<Key, Value>> iteratorClass) {
        is = new IteratorSetting(priority, name, iteratorClass)
        is.addOptions(properties.collectEntries { [it.key, it.value.toString()] })
    }

    @Override
    void setProperty(String name, Object value) {
        is.addOption(name, value.toString())
    }

    @Override
    Object getProperty(String name) {
        is.getOptions().get(name) ?: super.getProperty(name)
    }

    @Override
    protected void setParent(Object parent, Object child) {

    }

    @Override
    protected Object createNode(Object name) {
        if (name != "call")
            is.addOption(name.toString(), "1")
        return is
    }

    @Override
    protected Object createNode(Object name, Object value) {
        is.addOption(name.toString(), value.toString())
        return is
    }

    @Override
    protected Object createNode(Object name, Map attributes) {
        if (name == "call") {
            //noinspection GroovyAssignabilityCheck
            is.addOptions(attributes.collectEntries { k, v -> [k.toString(), v.toString()] })
        }
        return is
    }

    @Override
    protected Object createNode(Object name, Map attributes, Object value) {
        throw new IllegalArgumentException("Unexpected object passed to builder: ${name}")
    }
}
