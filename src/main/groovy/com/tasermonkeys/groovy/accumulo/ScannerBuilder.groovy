/*
   Copyright 2013 James Stapleton

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package com.tasermonkeys.groovy.accumulo

import com.tasermonkeys.groovy.accumulo.internal.BaseScannerBuilder
import com.tasermonkeys.groovy.accumulo.internal.Scanner_T

class ScannerBuilder extends BaseScannerBuilder {
    ScannerBuilder(AccumuloConnection connection) {
        super(connection)
    }

    @Override
    protected Object createNode(Object name) {
        if (name in Scanner_T.names) {
            return new Scanner_T(null)
        } else {
            return createNode(name)
        }

    }

    @Override
    protected Object createNode(Object name, Map attributes) {
        if (name in Scanner_T.names) {
            return new Scanner_T(connection, attributes)
        } else {
            return super.createNode(name, attributes)
        }
    }
}
