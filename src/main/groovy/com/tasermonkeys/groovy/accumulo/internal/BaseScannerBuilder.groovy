/*
 *  Copyright 2013 James Stapleton
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.tasermonkeys.groovy.accumulo.internal

import com.tasermonkeys.groovy.accumulo.AccumuloConnection
import com.tasermonkeys.groovy.accumulo.ScanIteratorSettingBuilder

class BaseScannerBuilder extends BuilderSupport {
    protected def AccumuloConnection connection

    BaseScannerBuilder(AccumuloConnection connection) {
        this.connection = connection
    }

    @Override
    protected void setParent(Object parent, Object child) {
        // this will be added onCompletion of the event
        if (child instanceof ScanIteratorSettingBuilder) return;

        parent.addItem(child)
    }

    @Override
    protected Object createNode(Object name) {
        if (name == Range_T.name) {
            return new Range_T(null)
        } else if (name in ScanIteratorSettingBuilder.names) {
            return new ScanIteratorSettingBuilder([]) // will throw error but this way error is generated in a central loc
        }
        return name
    }

    @Override
    protected Object createNode(Object name, Object value) {
        if (name == Range_T.name) {
            return new Range_T(value)
        } else if (name == Fetch_t.name) {
            return new Fetch_t(columnFamily: value)
        } else if (name in ScanIteratorSettingBuilder.names) {
            return new ScanIteratorSettingBuilder(1, value as Class)
        } else {
            return new BProp(name: name, value: value)
        }
    }

    @Override
    protected Object createNode(Object name, Map attributes) {
        if (name == Range_T.name) {
            def x = new Range_T(attributes)
            return x
        } else if (name == Fetch_t.name) {
            return new Fetch_t(attributes)
        } else if (name in ScanIteratorSettingBuilder.names) {
            return new ScanIteratorSettingBuilder(attributes)
        }
        return name
    }

    @Override
    protected Object createNode(Object name, Map attributes, Object value) {
        return name
    }

    @Override
    protected void nodeCompleted(Object parent, Object node) {
        if (node instanceof ScanIteratorSettingBuilder) {
            if (!(parent instanceof Scanner_T || parent instanceof BatchScanner_T))
                throw new IllegalArgumentException("ScanIteratorSetting must be a child of the root")
            else
                parent.addScannerSetting(node.is)
        }
        super.nodeCompleted(parent, node)
    }

    @Override
    protected void setClosureDelegate(Closure closure, Object node) {
        if (node instanceof BuilderSupport) {
            closure.setDelegate(node);
            closure.resolveStrategy = Closure.DELEGATE_ONLY
        } else
            super.setClosureDelegate(closure, node)
    }
}
