/*
 *  Copyright 2013 James Stapleton
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.tasermonkeys.groovy.accumulo.internal

import org.apache.accumulo.core.client.IteratorSetting
import org.apache.accumulo.core.client.ScannerBase
import org.apache.accumulo.core.data.Key
import org.apache.accumulo.core.data.Value

import static com.tasermonkeys.groovy.accumulo.AccumuloUtils.closeQuietly

public class BaseScanner_T {
    def static name = 'scanner'
    def static names = ['call', 'scanner']
    def ScannerBase scanner
    List<IterableWrapper> resultOptions = [] // can fit a Limit_T and/or a Sort_T, in either order.  However, order changes the outcome

    public BaseScanner_T(ScannerBase scanner) {
        this.scanner = scanner
    }

    @SuppressWarnings("GroovyAssignabilityCheck")
    public def addItem(item) {
        if (item instanceof Fetch_t) {
            item.addToScanner scanner
            return
        } else if (item instanceof IterableWrapper) {
            resultOptions.add(item)
        }
        scanner."${item.name}" = item.value
    }

    public void addScannerSetting(IteratorSetting setting) {
        scanner.addScanIterator(setting)
    }

    public def scan(Closure closure) {
        try {
            closure(scanner)
        } finally {
            closeQuietly(scanner)
        }
    }

    @SuppressWarnings("GroovyUnusedDeclaration")
    public def call(Closure closure) {
        scan(closure)
    }

    public Iterable<Map.Entry<Key, Value>> wrapScannerIfNeeded(ScannerBase scanner) {
        if (resultOptions.isEmpty()) return scanner
        Iterable<Map.Entry<Key, Value>> retVal = scanner
        resultOptions.each {
            retVal = it.wrapIterable(retVal)
        }
        return retVal
    }

    /**
     * Will call the closure for each element, and then close the scanner
     * @param closure the closure applied on each element found
     */
    public void each(Closure closure) {
        DefaultAccumuloGroovyMethods.scannerEach(wrapScannerIfNeeded(scanner), closure)
    }
    /**
     * Will call the closure for each element, and then close the scanner
     * @param closure a Closure to operate on each item
     */
    public void eachWithIndex(Closure closure) {
        DefaultAccumuloGroovyMethods.scannerEachWithIndex(wrapScannerIfNeeded(scanner), closure)
    }
    /**
     * Will call the closure for each element, then close the scanner
     * @param closure a Closure to call to transform the scanner result
     * @return collection of elements of the type returned by the closure
     */
    public Collection collect(Closure closure) {
        DefaultAccumuloGroovyMethods.scannerCollect(wrapScannerIfNeeded(scanner), closure)
    }
    /**
     * Will call the closure for each element, then close the scanner
     * @param closure a Closure to call to transform the scanner result into a Map
     * @return map of the k,v elements of the type returned by the closure
     */
    public collectEntries(Closure closure) {
        DefaultAccumuloGroovyMethods.scannerCollectEntries(wrapScannerIfNeeded(scanner), closure)
    }

    /**
     * performs the scan, then counts then number of results, then close the scanner
     * @return the count of the number results
     */
    public count() {
        DefaultAccumuloGroovyMethods.scannerCount(wrapScannerIfNeeded(scanner), Closure.IDENTITY)
    }

    /**
     * Call closure for each element, then close the scanner
     * @param closure - return boolean if wanted to be counted towards the count
     * @return the count of the number of times the closure return true
     */
    public count(Closure closure) {
        DefaultAccumuloGroovyMethods.scannerCount(wrapScannerIfNeeded(scanner), closure)
    }
    /**
     * Sums the result of apply a closure to each item of the scanner.
     * <code>scanner.sum(closure)</code> is equivalent to:
     * <code>scanner.collect(closure).sum()</code>
     * however, it doesn't need to store all of the results intermediately to do so. So scanner is more efficient.
     *
     * This method then closes the scanner when done
     *
     * @param closure a single parameter closure that returns a numeric value.
     * @return The sum of the values returned by applying the closure to each
     *         result of the scanner.
     */
    public sum(Closure closure) {
        DefaultAccumuloGroovyMethods.scannerSum(wrapScannerIfNeeded(scanner), closure)
    }
    /**
     * This will close the scanner when done.
     *
     * @param comparator a Comparator
     * @return the entry to which the comparator results as the min value
     */
    public Map.Entry<Key, Value> min(Comparator<Map.Entry<Key, Value>> comparator) {
        DefaultAccumuloGroovyMethods.scannerMin(wrapScannerIfNeeded(scanner), comparator)
    }

    /**
     * Sums the result of apply a closure to each item of the scanner.
     * <code>scanner.sum(closure)</code> is equivalent to:
     * <code>scanner.collect(closure).sum()</code>
     * however, it doesn't need to store all of the results intermediately to do so. So this is more efficient.
     *
     * scanner method then closes the scanner when done
     *
     * @param closure a single parameter closure that returns a numeric value.
     * @param initialValue the closure results will be summed with this initial value
     * @return The sum of the values returned by applying the closure to each
     *         result of the scanner.
     */
    public sum(Object initialValue, Closure closure) {
        DefaultAccumuloGroovyMethods.scannerSum(wrapScannerIfNeeded(scanner), initialValue, closure)
    }

    /**
     * This will close the scanner when done.
     *
     * @param comparator a Comparator
     * @return the entry to which the comparator results as the max value
     */
    public Map.Entry<Key, Value> max(Comparator<Map.Entry<Key, Value>> comparator) {
        DefaultAccumuloGroovyMethods.scannerMax(wrapScannerIfNeeded(scanner), comparator)
    }

    /**
     * Concatenates the results of calling the closure of each
     * result of this scanner, with <code>","</code> as a separator between
     * each item.
     *
     * Will close the scanner when finished
     *
     * @param closure closure to call
     * @return the joined String ',' delimited
     */
    public String join(Closure closure) {
        DefaultAccumuloGroovyMethods.scannerJoin(wrapScannerIfNeeded(scanner), closure)
    }

    /**
     * Concatenates the results of calling the closure of each
     * result of this scanner, with the given String as a separator between
     * each item.
     *
     * Will close the scanner when finished
     *
     * @param closure closure to call
     * @param separator the delimiter to put in between each item
     * @return the joined String ',' delimited
     */
    public String join(String separator, Closure closure) {
        DefaultAccumuloGroovyMethods.scannerJoin(wrapScannerIfNeeded(scanner), separator, closure)
    }

    /**
     * Iterates over the results of a scanner, and checks whether at least
     * one call to the closure returned true
     *
     * Will close the scanner when finished
     *
     * @param closure the 1 or 2 arg closure predicate used for matching
     * @return true if any result in the scanner matches the closure predicate
     */
    public boolean any(Closure closure) {
        DefaultAccumuloGroovyMethods.scannerAny(wrapScannerIfNeeded(scanner), closure)
    }

    /**
     * Iterates over the results of a scanner, and checks whether all
     * calls to the closure returned true
     *
     * Will close the scanner when finished
     *
     * @param closure the 1 or 2 arg closure predicate used for matching
     * @return true if all results in the scanner matches the closure predicate
     */
    public boolean every(Closure closure) {
        DefaultAccumuloGroovyMethods.scannerEvery(wrapScannerIfNeeded(scanner), closure)
    }

    /**
     * Finds the first result matching the closure condition.
     *
     * Closes the scanner when finished
     *
     * @param closure a closure condition
     * @return the first Result found
     */
    public find(Closure closure) {
        DefaultAccumuloGroovyMethods.scannerFind(wrapScannerIfNeeded(scanner), closure)
    }

    /**
     * Finds all scanner results matching the closure condition.
     *
     * Closes the scanner when finished
     *
     * @param closure a closure condition
     * @return a Collection of matching values
     */
    public findAll(Closure closure) {
        DefaultAccumuloGroovyMethods.scannerFindAll(wrapScannerIfNeeded(scanner), closure)
    }

    /**
     * Splits all results into two collections based on the closure condition.
     * The first list contains all results which match the closure expression.
     * The second list all those that don't.
     *
     * Closes the scanner when finished
     *
     * @param closure a closure condition
     * @return a List whose first item is the accepted values and whose second item is the rejected values
     */

    public split(Closure closure) {
        DefaultAccumuloGroovyMethods.scannerSplit(wrapScannerIfNeeded(scanner), closure)
    }

    /**
     * Sorts all scanner results into groups determined by the
     * supplied mapping closure.  The closure should return the key that this
     * item should be grouped by.  The returned LinkedHashMap will have an entry for each
     * distinct key returned from the closure, with each value being a list of
     * items for that group.
     * <p>
     *     Scanner is closed when finished.
     *
     * @param closure a closure mapping entries on keys
     * @return a Map grouped by keys
     */
    public <K> Map<K, List<Map.Entry<Key, Value>>> groupBy(Closure<K> closure) {
        DefaultAccumuloGroovyMethods.scannerGroupBy(wrapScannerIfNeeded(scanner), closure)
    }

    /**
     * Sorts all scanner results into (sub)groups determined by the supplied
     * mapping closures. Each closure should return the key that this item
     * should be grouped by. The returned LinkedHashMap will have an entry for each
     * distinct 'key path' returned from the closures, with each value being a list
     * of items for that 'group path'. <p/>
     *
     * If an empty array of closures is supplied the IDENTITY Closure will be used.
     * <p/>
     * Closes the scanner when finished
     *
     * @param closures an array of closures, each mapping entries on keys
     * @return a Map grouped by keys on each criterion
     */
    public Map groupBy(Object... closures) {
        DefaultAccumuloGroovyMethods.scannerGroupBy(wrapScannerIfNeeded(scanner), closures)
    }

    /**
     * Sorts all scanner results into (sub)groups determined by the supplied
     * mapping closures. Each closure should return the key that this item
     * should be grouped by. The returned LinkedHashMap will have an entry for each
     * distinct 'key path' returned from the closures, with each value being a list
     * of items for that 'group path'. <p/>
     *
     * If an empty List of closures is supplied the IDENTITY Closure will be used.
     * <p/>
     * Closes the scanner when finished
     *
     * @param closures a List of closures, each mapping entries on keys
     * @return a Map grouped by keys on each criterion
     */
    public Map groupBy(List<Closure> closures) {
        DefaultAccumuloGroovyMethods.scannerGroupBy(wrapScannerIfNeeded(scanner), closures)
    }
}
