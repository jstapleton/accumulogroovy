/*
 *  Copyright 2013 James Stapleton
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.tasermonkeys.groovy.accumulo.internal;

import com.tasermonkeys.groovy.accumulo.AccumuloUtils;
import groovy.lang.Closure;
import groovy.lang.MetaClass;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;
import org.codehaus.groovy.runtime.DefaultGroovyMethods;
import org.codehaus.groovy.runtime.InvokerHelper;
import org.codehaus.groovy.runtime.callsite.BooleanClosureWrapper;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"UnusedDeclaration", "unchecked", "RedundantArrayCreation"})
public class DefaultAccumuloGroovyMethods extends DefaultGroovyMethods {

    protected static <T> T callClosureForMapEntryAndCounter(Closure<T> closure, Map.Entry entry, int counter) {
        if (closure.getMaximumNumberOfParameters() == 3) {
            return closure.call(new Object[]{entry.getKey(), entry.getValue(), counter});
        }
        if (closure.getMaximumNumberOfParameters() == 2) {
            return closure.call(new Object[]{entry, counter});
        }
        return closure.call(entry);
    }

    protected static <T> T callClosureForMapEntry(Closure<T> closure, Map.Entry entry) {
        if (closure.getMaximumNumberOfParameters() == 2) {
            return closure.call(new Object[]{entry.getKey(), entry.getValue()});
        }
        return closure.call(entry);
    }


    public static void scannerEach(Iterable<Map.Entry<Key, Value>> scanner, Closure closure) {
        try {
            DefaultGroovyMethods.each(scanner, closure);
        } finally {
            AccumuloUtils.closeQuietly(scanner);
        }
    }

    public static void scannerEachWithIndex(Iterable<Map.Entry<Key, Value>> scanner, Closure closure) {
        try {
            DefaultGroovyMethods.eachWithIndex(scanner, closure);
        } finally {
            AccumuloUtils.closeQuietly(scanner);
        }
    }

    public static Object scannerCollect(Iterable<Map.Entry<Key, Value>> scanner, Closure closure) {
        try {
            return DefaultGroovyMethods.collect(scanner, closure);
        } finally {
            AccumuloUtils.closeQuietly(scanner);
        }
    }

    public static Object scannerCollectEntries(Iterable<Map.Entry<Key, Value>> scanner, Closure closure) {
        try {
            return DefaultGroovyMethods.collectEntries(scanner.iterator(), closure);
        } finally {
            AccumuloUtils.closeQuietly(scanner);
        }
    }

    /**
     * @param closure - return boolean if wanted to be counted towards the count
     * @return the count of the number of times the closure return true
     */
    public static Object scannerCount(Iterable<Map.Entry<Key, Value>> scanner, Closure closure) {
        try {
            return DefaultGroovyMethods.count(scanner.iterator(), closure);
        } finally {
            AccumuloUtils.closeQuietly(scanner);
        }
    }

    public static Object scannerSum(Iterable<Map.Entry<Key, Value>> scanner, Closure closure) {
        return scannerSum(scanner, null, closure, true);
    }

    public static Object scannerSum(Iterable<Map.Entry<Key, Value>> scanner, Object initialValue, Closure closure) {
        try {
            return scannerSum(scanner, initialValue, closure, false);
        } finally {
            AccumuloUtils.closeQuietly(scanner);
        }
    }

    private static Object scannerSum(Iterable<Map.Entry<Key, Value>> self, Object initialValue, Closure closure, boolean first) {
        Object result = initialValue;
        Object[] closureParam = new Object[1];
        Object[] plusParam = new Object[1];
        for (Map.Entry<Key, Value> next : self) {
            closureParam[0] = next;
            plusParam[0] = closure.call(closureParam);
            if (first) {
                result = plusParam[0];
                first = false;
                continue;
            }
            MetaClass metaClass = InvokerHelper.getMetaClass(result);
            result = metaClass.invokeMethod(result, "plus", plusParam);
        }
        return result;
    }

    /**
     * Selects the minimum value found in the scan using the given comparator.
     *
     * @param scanner    a scanner to scan over
     * @param comparator a Comparator
     * @return the minimum value
     */
    public static Map.Entry<Key, Value> scannerMin(Iterable<Map.Entry<Key, Value>> scanner, Comparator<Map.Entry<Key, Value>> comparator) {
        try {
            Map.Entry<Key, Value> answer = null;
            for (Map.Entry<Key, Value> value : scanner) {
                if (answer == null || comparator.compare(value, answer) < 0) {
                    answer = value;
                }
            }
            return answer;
        } finally {
            AccumuloUtils.closeQuietly(scanner);
        }
    }

    /**
     * Selects the maximum value found in the scan using the given comparator.
     *
     * @param scanner    a scanner to scan over
     * @param comparator a Comparator
     * @return the maximum value
     */
    public static Map.Entry<Key, Value> scannerMax(Iterable<Map.Entry<Key, Value>> scanner, Comparator<Map.Entry<Key, Value>> comparator) {
        try {
            Map.Entry<Key, Value> answer = null;
            for (Map.Entry<Key, Value> value : scanner) {
                if (answer == null || comparator.compare(value, answer) > 0) {
                    answer = value;
                }
            }
            return answer;
        } finally {
            AccumuloUtils.closeQuietly(scanner);
        }
    }

    public static String scannerJoin(Iterable<Map.Entry<Key, Value>> scanner, Closure closure) {
        return scannerJoin(scanner, ",", closure);
    }

    public static String scannerJoin(Iterable<Map.Entry<Key, Value>> scanner, String separator, Closure closure) {
        try {
            StringBuilder buffer = new StringBuilder();
            boolean first = true;

            if (separator == null) separator = "";

            for (Map.Entry<Key, Value> value : scanner) {
                if (first) {
                    first = false;
                } else {
                    buffer.append(separator);
                }
                buffer.append(InvokerHelper.toString(callClosureForMapEntry(closure, value)));
            }
            return buffer.toString();
        } finally {
            AccumuloUtils.closeQuietly(scanner);
        }
    }

    public static boolean scannerAny(Iterable<Map.Entry<Key, Value>> scanner, Closure<?> closure) {
        try {
            BooleanClosureWrapper bcw = new BooleanClosureWrapper(closure);
            for (Map.Entry<Key, Value> entry : scanner) {
                if (bcw.callForMap(entry)) {
                    return true;
                }
            }
            return false;
        } finally {
            AccumuloUtils.closeQuietly(scanner);
        }
    }

    public static boolean scannerEvery(Iterable<Map.Entry<Key, Value>> scanner, Closure closure) {
        try {
            BooleanClosureWrapper bcw = new BooleanClosureWrapper(closure);
            for (Map.Entry<Key, Value> entry : scanner) {
                if (!bcw.callForMap(entry)) {
                    return false;
                }
            }
            return true;
        } finally {
            AccumuloUtils.closeQuietly(scanner);
        }
    }

    public static Object scannerFind(Iterable<Map.Entry<Key, Value>> scanner, Closure closure) {
        try {
            return DefaultGroovyMethods.find(scanner, closure);
        } finally {
            AccumuloUtils.closeQuietly(scanner);
        }
    }

    public static Object scannerFindAll(Iterable<Map.Entry<Key, Value>> scanner, Closure closure) {
        try {
            return DefaultGroovyMethods.findAll(scanner, closure);
        } finally {
            AccumuloUtils.closeQuietly(scanner);
        }
    }

    public static Object scannerSplit(Iterable<Map.Entry<Key, Value>> scanner, Closure closure) {
        try {
            return DefaultGroovyMethods.split(scanner, closure);
        } finally {
            AccumuloUtils.closeQuietly(scanner);
        }
    }


    /**
     * Sorts all scanner results into groups determined by the
     * supplied mapping closure.  The closure should return the key that this
     * item should be grouped by.  The returned LinkedHashMap will have an entry for each
     * distinct key returned from the closure, with each value being a list of
     * items for that group.
     * <p/>
     * Example usage:
     * <pre class="groovyTestCase">assert [0:[2,4,6], 1:[1,3,5]] == [1,2,3,4,5,6].groupBy { it % 2 }</pre>
     *
     * @param scanner a scanner whose results to group
     * @param closure a closure mapping entries on keys
     * @return a new Map grouped by keys
     * @since 1.0
     */
    public static <K> Map<K, List<Map.Entry<Key, Value>>> scannerGroupBy(Iterable<Map.Entry<Key, Value>> scanner, Closure<K> closure) {
        try {
            Map<K, List<Map.Entry<Key, Value>>> answer = new LinkedHashMap<K, List<Map.Entry<Key, Value>>>();
            for (Map.Entry<Key, Value> element : scanner) {
                K value = closure.call(element);
                groupAnswer(answer, element, value);
            }
            return answer;
        } finally {
            AccumuloUtils.closeQuietly(scanner);
        }
    }

    /**
     * Sorts all scanner results into (sub)groups determined by the supplied
     * mapping closures. Each closure should return the key that this item
     * should be grouped by. The returned LinkedHashMap will have an entry for each
     * distinct 'key path' returned from the closures, with each value being a list
     * of items for that 'group path'. <p>
     * <p/>
     * Example usage:
     * <pre class="groovyTestCase">def result = [1,2,3,4,5,6].groupBy({ it % 2 }, { it < 4 })
     * assert result == [1:[(true):[1, 3], (false):[5]], 0:[(true):[2], (false):[4, 6]]]</pre>
     * <p/>
     * Another example:
     * <pre>def sql = groovy.sql.Sql.newInstance(/&ast; ... &ast;/)
     * def data = sql.rows("SELECT * FROM a_table").groupBy({ it.column1 }, { it.column2 }, { it.column3 })
     * if (data.val1.val2.val3) {
     * // there exists a record where:
     * //   a_table.column1 == val1
     * //   a_table.column2 == val2, and
     * //   a_table.column3 == val3
     * } else {
     * // there is no such record
     * }</pre>
     * If an empty array of closures is supplied the IDENTITY Closure will be used.
     *
     * @param scanner  a scanner whose results to group
     * @param closures an array of closures, each mapping entries on keys
     * @return a new Map grouped by keys on each criterion
     * @see Closure#IDENTITY
     * @since 1.8.1
     */
    public static Map scannerGroupBy(Iterable<Map.Entry<Key, Value>> scanner, Object... closures) {
        final Closure head = closures.length == 0 ? Closure.IDENTITY : (Closure) closures[0];

        @SuppressWarnings("unchecked")
        Map<Object, List> first = scannerGroupBy(scanner, head);
        if (closures.length < 2)
            return first;

        final Object[] tail = new Object[closures.length - 1];
        System.arraycopy(closures, 1, tail, 0, closures.length - 1); // Arrays.copyOfRange only since JDK 1.6

        // inject([:]) { a,e -> a << [(e.key): e.value.groupBy(tail)] }
        Map<Object, Map> acc = new LinkedHashMap<Object, Map>();
        for (Map.Entry<Object, List> item : first.entrySet()) {
            acc.put(item.getKey(), groupBy(item.getValue(), tail));
        }

        return acc;
    }

    /**
     * Sorts all scanner results into (sub)groups determined by the supplied
     * mapping closures. Each closure should return the key that this item
     * should be grouped by. The returned LinkedHashMap will have an entry for each
     * distinct 'key path' returned from the closures, with each value being a list
     * of items for that 'group path'. <p>
     * <p/>
     * Example usage:
     * <pre class="groovyTestCase">def result = [1,2,3,4,5,6].groupBy([{ it % 2 }, { it < 4 }])
     * assert result == [1:[(true):[1, 3], (false):[5]], 0:[(true):[2], (false):[4, 6]]]</pre>
     * <p/>
     * Another example:
     * <pre>def sql = groovy.sql.Sql.newInstance(/&ast; ... &ast;/)
     * def data = sql.rows("SELECT * FROM a_table").groupBy([{ it.column1 }, { it.column2 }, { it.column3 }])
     * if (data.val1.val2.val3) {
     * // there exists a record where:
     * //   a_table.column1 == val1
     * //   a_table.column2 == val2, and
     * //   a_table.column3 == val3
     * } else {
     * // there is no such record
     * }</pre>
     * If an empty list of closures is supplied the IDENTITY Closure will be used.
     *
     * @param scanner  a scanner whose results to group
     * @param closures a list of closures, each mapping entries on keys
     * @return a new Map grouped by keys on each criterion
     * @see Closure#IDENTITY
     * @since 1.8.1
     */
    public static Map scannerGroupBy(Iterable<Map.Entry<Key, Value>> scanner, List<Closure> closures) {
        return scannerGroupBy(scanner, closures.toArray());
    }


}
