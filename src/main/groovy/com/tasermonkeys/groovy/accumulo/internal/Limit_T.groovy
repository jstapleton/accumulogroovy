/*
 *  Copyright 2013 James Stapleton
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.tasermonkeys.groovy.accumulo.internal

import org.apache.accumulo.core.data.Key
import org.apache.accumulo.core.data.Value

public class Limit_T implements IterableWrapper {
    def static name = 'limit'
    def long pageSize = -1;
    def long pageStart = 0;

    public Limit_T(value) {
        if (isNumber(value)) {
            pageSize = Long.valueOf(value)
            pageStart = 0
        } else if (value instanceof Map) {
            setFromMap(value)
        }
    }

    public Limit_T(Map params) {
        setFromMap(params)
    }

    public Iterable<Map.Entry<Key, Value>> wrapIterable(Iterable<Map.Entry<Key, Value>> toWrap) {
        return new Limiter(toWrap, this)
    }

    private void setFromMap(Map params) {
        if (params.containsKey('pageStart')) {
            pageStart = Long.valueOf(params['pageStart'])
        }
        if (params.containsKey('pageEnd')) {
            pageSize = Long.valueOf(params['pageEnd']) - pageStart
        }
        if (params.containsKey('pageSize')) {
            if (pageSize != -1) throw new IllegalArgumentException("Both pageSize and pageEnd can not be specified")
            pageSize = Long.valueOf(params['pageSize'])
        }
        if (pageSize < 0) {
            throw new IllegalArgumentException("Invalid page size or pageEnd is before pageStart")
        }
    }

    private static boolean isNumber(value) {
        value instanceof Integer || value instanceof Byte || value instanceof Long || value instanceof Short ||
                (value instanceof String && value.isNumber())
    }
}
