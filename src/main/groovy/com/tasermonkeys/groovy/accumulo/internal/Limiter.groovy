/*
 *  Copyright 2013 James Stapleton
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.tasermonkeys.groovy.accumulo.internal

import com.tasermonkeys.groovy.accumulo.AccumuloUtils
import org.apache.accumulo.core.data.Key
import org.apache.accumulo.core.data.Value

/**
 * Is there an iterator that can do this job for me?
 * What about if I wanted to sort and then return only the first N rows?
 */
class Limiter implements Iterable<Map.Entry<Key, Value>>, Closeable {

    Iterable<Map.Entry<Key, Value>> scanner
    Limit_T limit

    public Limiter(Iterable<Map.Entry<Key, Value>> scanner, Limit_T limit) {
        this.scanner = scanner
        this.limit = limit
    }

    @Override
    Iterator<Map.Entry<Key, Value>> iterator() {
        return new MyIterator()
    }

    public class MyIterator implements Iterator<Map.Entry<Key, Value>> {
        long count = 0;
        Iterator<Map.Entry<Key, Value>> iterator

        MyIterator() {
            iterator = scanner.iterator()
            // skip over the first limit.pageStart entries or till end whichever is first
            for (int numToSkip = 0; numToSkip < limit.pageStart && iterator.hasNext(); numToSkip++) {
            }
        }

        @Override
        boolean hasNext() {
            return (count < limit.pageSize || limit.pageSize == 0) && iterator.hasNext()
        }

        @Override
        Map.Entry<Key, Value> next() {
            if (!hasNext()) throw new NoSuchElementException()
            count++
            return iterator.next()
        }

        @Override
        void remove() {
            iterator.remove()
        }
    }

    @Override
    void close() throws IOException {
        AccumuloUtils.closeQuietly(scanner)
    }
}
