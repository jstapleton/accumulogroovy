/*
 *  Copyright 2013 James Stapleton
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.tasermonkeys.groovy.accumulo.internal

import org.apache.accumulo.core.data.Key
import org.apache.accumulo.core.data.Range
import org.apache.hadoop.io.Text

import static com.tasermonkeys.groovy.accumulo.AccumuloUtils.asKey

public class Range_T {
    def static name = 'range'
    def Range range

    Range_T(rowId) {
        if (rowId instanceof Text)
            range = new Range(rowId as Text)
        else
            range = new Range(rowId.toString())

    }

    Range_T(String start, String end) {
        range = new Range(start, end)
    }

    Range_T(Text start, Text end) {
        range = new Range(start, end)
    }

    Range_T(Key start, Key end) {
        range = new Range(start, end)
    }

    Range_T(Map map) {
        Key startKey = asKey(map['start'])
        Key stopKey = asKey(map['stop'])

        boolean startRowInclusive = map['startRowInclusive'] != null ? map['startRowInclusive'] as boolean : true
        boolean endRowInclusive = map['endRowInclusive'] != null ? map['endRowInclusive'] as boolean : true
        range = new Range(startKey, startRowInclusive, stopKey, endRowInclusive)
    }

}