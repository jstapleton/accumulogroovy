/*
 *  Copyright 2013 James Stapleton
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.tasermonkeys.groovy.accumulo.internal


public class Ranges_T {
    def static name = 'ranges'
    def ranges = []

    Ranges_T(Map attributes) {
        if (attributes != null) throw new IllegalArgumentException("Ranges doesn't expect arguments")
    }

    def addItem(item) {
        if (!(item instanceof Range_T)) {
            throw new IllegalArgumentException("Expecting only range to be added to ranges")
        }
        ranges.add item
    }
}
