/*
 *  Copyright 2013 James Stapleton
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.tasermonkeys.groovy.accumulo.internal

import com.tasermonkeys.groovy.accumulo.AccumuloConnection
import org.apache.accumulo.core.client.Scanner

public class Scanner_T extends BaseScanner_T {
    def Scanner singleScanner

    public Scanner_T(AccumuloConnection connection, Map params) {
        super(connection.createScanner(params))
        singleScanner = (Scanner) scanner
    }

    def addItem(item) {
        if (item instanceof Range_T) {
            singleScanner.range = item.range
        } else {
            super.addItem(item)
        }
    }
}