/*
 *  Copyright 2013 James Stapleton
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.tasermonkeys.groovy.accumulo.internal

import org.apache.accumulo.core.data.Key
import org.apache.accumulo.core.data.Value

public class Sort_T implements IterableWrapper {
    Comparator comparator;


    public Sort_T(Map params) {
        def reverse = params['reverse'] ?: false
        def sortBy = params['sortBy']
        if (sortBy) {
            comparator = makeComparator(sortBy, reverse)
        } else {
            comparator = makeComparator("key", reverse)
        }
    }

    public Iterable<Map.Entry<Key, Value>> wrapIterable(Iterable<Map.Entry<Key, Value>> toWrap) {
        return new Sorter(toWrap, this)
    }

    public static Comparator makeComparator(def sortBy, boolean reverse) {
        if (!(sortBy instanceof List)) {
            sortBy = [sortBy]
        }
        def sortSpec = sortBy.collect { item ->
            if (item instanceof String) {
                switch (item.toLowerCase()) {
                    case "rowid":
                        return { Map.Entry<Key, Value> a -> a.key.row }
                    case "columnfamily":
                        return { Map.Entry<Key, Value> a -> a.key.columnFamily }
                    case "columnqualifier":
                        return { Map.Entry<Key, Value> a -> a.key.columnQualifier }
                    case "key":
                        return { Map.Entry<Key, Value> a -> a.key }
                }
            } else if (item instanceof Closure) {
                return (Closure) item
            }
        }
        if (!reverse)
            return new OrderBy(sortSpec)
        else
            return new ReverseOrderBy(sortSpec)
    }
}
