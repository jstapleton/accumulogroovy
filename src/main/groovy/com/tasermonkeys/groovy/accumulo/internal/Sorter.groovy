/*
 *  Copyright 2013 James Stapleton
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.tasermonkeys.groovy.accumulo.internal

import com.tasermonkeys.groovy.accumulo.AccumuloUtils
import org.apache.accumulo.core.data.Key
import org.apache.accumulo.core.data.Value

class Sorter implements Iterable<Map.Entry<Key, Value>>, Closeable {

    Iterable<Map.Entry<Key, Value>> scanner
    Comparator comparator

    public Sorter(Iterable<Map.Entry<Key, Value>> scanner, Comparator sortSpec) {
        this.comparator = sortSpec
        this.scanner = scanner
    }

    public Sorter(Iterable<Map.Entry<Key, Value>> scanner, Sort_T sortSpec) {
        this(sortSpec.comparator)
    }

    @Override
    void close() throws IOException {
        AccumuloUtils.closeQuietly(scanner)
    }

    @Override
    Iterator<Map.Entry<Key, Value>> iterator() {
        // Could we do this on the server side? So that we don't have to bring it all into memory at once in order to
        // sort this?  Also is there a way not to have to bring it all into memory in order to sort it?
        scanner.iterator().sort(comparator).iterator()
    }
}
