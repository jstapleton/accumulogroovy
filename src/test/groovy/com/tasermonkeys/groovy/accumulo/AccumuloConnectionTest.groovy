/*
   Copyright 2013 James Stapleton

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package com.tasermonkeys.groovy.accumulo

import org.apache.accumulo.core.client.BatchScanner
import org.apache.accumulo.core.client.Scanner
import org.apache.accumulo.core.data.Key
import org.apache.accumulo.core.data.Mutation
import org.apache.accumulo.core.data.Value

class AccumuloConnectionTest extends GroovyTestCase {
    def params = [
            instanceName: "instance",
            zookeeper: ["a", "b", "c"],
            username: "test",
            password: "password",
            useMock: true,
            tablePrefix: "prefix"

    ]

    public class TestCallable {
        def called = false

        public def call() {
            called = true
            this
        }

        public def call(Closure closure) {
            closure("hello")
            called = true
            this
        }
    }

    void testFooBar() {
        def foo = new TestCallable()
        foo()
        assert foo.called
        foo.called = false
        def cCalled = false
        assert foo {
            cCalled = true
        }.called
        assert cCalled

    }

    def static sampleMutation(Map params) {
        def m = new Mutation(params["rowId"].toString())
        m.put(params["cf"].toString(), params["cq"].toString(), new Value(params["value"].toString().getBytes()))
        return m
    }

    void testWithBatchScanner() {
        AccumuloConnection connection = new AccumuloConnection(params)
        connection.createTableIfNeeded("testScanner")
        def writer = connection.createWriter(tableName: "testBatchScanner")
        writer.addMutation(sampleMutation(rowId: "apple", cf: "family", cq: "qual", value: "A value"))
        writer.flush()
        writer.close()
        def scanCalled = false
        def foundItems =
            connection.withBatchScanner(tableName: "testBatchScanner", authorizations: ["a", "b", "c"]) {
                range(start: "a", stop: "b")
            } {
                BatchScanner scanner ->
                    scanCalled = true
                    def foundItems = []
                    for (def x : scanner) {
                        foundItems.add([rowId: x.key.row.toString(), cf: x.key.columnFamily.toString(),
                                cq: x.key.columnQualifier.toString(), visibility: x.key.columnVisibility?.toString(), value: x.value.toString()])
                    }
                    foundItems
            }
        assert scanCalled
        assert foundItems == [[rowId: "apple", cf: "family", cq: "qual",
                visibility: "", value: "A value"]]

    }

    void testWithScanner() {
        AccumuloConnection connection = new AccumuloConnection(params)
        connection.createTableIfNeeded("testScanner")
        def writer = connection.createWriter(tableName: "testScanner")
        writer.addMutation(sampleMutation(rowId: "apple", cf: "family", cq: "qual", value: "A value"))
        writer.flush()
        writer.close()
        def scanCalled = false
        def foundItems =
            connection.withScanner(tableName: "testScanner", authorizations: ["a", "b", "c"]) {
                range(start: "a", stop: "b")
            } {
                Scanner scanner ->
                    scanCalled = true
                    def foundItems = []
                    for (def x : scanner) {
                        foundItems.add([rowId: x.key.row.toString(), cf: x.key.columnFamily.toString(),
                                cq: x.key.columnQualifier.toString(), visibility: x.key.columnVisibility?.toString(),
                                value: x.value.toString()])
                    }
                    foundItems
            }
        assert scanCalled
        assert foundItems == [[rowId: "apple", cf: "family", cq: "qual",
                visibility: "", value: "A value"]]
    }

    void testScannerExamples() {
        def connection = new MockAccumuloConnection(params)
        // print rowId => value for each result
        connection.withScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.each { Key k, Value v ->
            println k.row.toString() + "=" + v.toString()
        }
        // convert the results into a collection of strings of the value
        connection.withScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.collect { Key k, Value v ->
            v.toString()
        }

        // returns the number of results return by the scan definition
        connection.withScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.count()

        // count all results whoes columnFamily equals "cf"... This could have been done with a fetchColumn, then count
        connection.withScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.count {
            Key k, v -> k.columnFamily.equals("cf")
        }

        // a comma separated string of each value as a string
        connection.withScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.join() {
            it.value.toString()
        }

        // a semi-colon separated string of each value as a string
        connection.withScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.join(";") {
            it.value.toString()
        }

        // A map of [rowId as String] = value as String
        connection.withScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.collectEntries {
            Map.Entry<Key, Value> it -> [it.key.row.toString(), it.value.toString()]
        }

        // sum up the values as if they were all UTF-8 text of numbers
        connection.withScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.sum {
            k, Value v -> v.toString().toInteger()
        }

        // sum up the values as if they were all UTF-8 text of numbers start with 42
        connection.withScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.sum(42) {
            k, Value v -> v.toString().toInteger()
        }

        // find the min value if all were UTF-8 text of numbers
        connection.withScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.min({
            a, b -> a.value.toString().toInteger() <=> b.value.toString().toInteger()
        } as Comparator)

        // find the max value if all were UTF-8 text of numbers
        connection.withScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.max({
            a, b -> a.value.toString().toInteger() <=> b.value.toString().toInteger()
        } as Comparator)

        // group by the columnFamily
        // the returning map would be in the form of m['cf'] = Map.Entry<Key, Value>
        connection.withScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.groupBy {
            Key k, v -> k.columnFamily.toString()
        }

        // group by the columnFamily, then columnQualifier...
        // the returning map would be in the form of m['cf']['cq'] = Map.Entry<Key, Value>
        connection.withScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.groupBy {
            Key k, v -> k.columnFamily.toString()
        } {
            Key k, v -> k.columnQualifier
        }

        // group by the columnFamily, then columnQualifier...
        // the returning map would be in the form of m['cf']['cq'] = Map.Entry<Key, Value>
        connection.withScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.groupBy([{
            Key k, v -> k.columnFamily.toString()
        }, {
            Key k, v -> k.columnQualifier
        }])

    }

    void testBatchScannerExamples() {
        def connection = new MockAccumuloConnection(params)
        // print rowId => value for each result
        connection.withBatchScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.each { Key k, Value v ->
            println k.row.toString() + "=" + v.toString()
        }
        // convert the results into a collection of strings of the value
        connection.withBatchScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.collect { Key k, Value v ->
            v.toString()
        }

        // returns the number of results return by the scan definition
        connection.withBatchScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.count()

        // count all results whoes columnFamily equals "cf"... This could have been done with a fetchColumn, then count
        connection.withBatchScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.count {
            Key k, v -> k.columnFamily.equals("cf")
        }

        // a comma separated string of each value as a string
        connection.withBatchScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.join {
            it.value.toString()
        }

        // a semi-colon separated string of each value as a string
        connection.withBatchScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.join(";") {
            it.value.toString()
        }

        // A map of [rowId as String] = value as String
        connection.withBatchScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.collectEntries {
            Map.Entry<Key, Value> it -> [it.key.row.toString(), it.value.toString()]
        }

        // sum up the values as if they were all UTF-8 text of numbers
        connection.withBatchScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.sum {
            k, Value v -> v.toString().toInteger()
        }

        // sum up the values as if they were all UTF-8 text of numbers start with 42
        connection.withBatchScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.sum(42) {
            k, Value v -> v.toString().toInteger()
        }

        // find the min value if all were UTF-8 text of numbers
        connection.withBatchScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.min({
            a, b -> a.value.toString().toInteger() <=> b.value.toString().toInteger()
        } as Comparator)

        // find the max value if all were UTF-8 text of numbers
        connection.withBatchScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.max({
            a, b -> a.value.toString().toInteger() <=> b.value.toString().toInteger()
        } as Comparator)

        // group by the columnFamily
        // the returning map would be in the form of m['cf'] = Map.Entry<Key, Value>
        connection.withBatchScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.groupBy {
            Key k, v -> k.columnFamily.toString()
        }

        // group by the columnFamily, then columnQualifier...
        // the returning map would be in the form of m['cf']['cq'] = Map.Entry<Key, Value>
        connection.withBatchScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.groupBy {
            Key k, v -> k.columnFamily.toString()
        } {
            Key k, v -> k.columnQualifier
        }

        // group by the columnFamily, then columnQualifier...
        // the returning map would be in the form of m['cf']['cq'] = Map.Entry<Key, Value>
        connection.withBatchScanner(tableName: "examples", authorizations: ["a", "b", "c"]) {
            range(start: "a", end: "b")
        }.groupBy([{
            Key k, v -> k.columnFamily.toString()
        }, {
            Key k, v -> k.columnQualifier
        }])

    }


    void testGetTableName() {
        AccumuloConnection connection = new AccumuloConnection(params)
        assert connection.getTableName("something") == "prefix_something"
        def noPrefixParams = [
                instanceName: "instance",
                zookeeper: ["a", "b", "c"],
                username: "test",
                password: "password",
                useMock: true,
        ]
        connection = new AccumuloConnection(noPrefixParams)
        assert connection.getTableName("something") == "something"
    }
}
