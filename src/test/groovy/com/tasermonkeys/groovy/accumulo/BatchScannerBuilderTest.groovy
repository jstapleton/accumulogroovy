/*
   Copyright 2013 James Stapleton

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package com.tasermonkeys.groovy.accumulo

import org.apache.accumulo.core.data.Column
import org.apache.accumulo.core.data.Key
import org.apache.accumulo.core.data.Range
import org.apache.accumulo.core.data.thrift.IterInfo
import org.apache.accumulo.core.iterators.user.AgeOffFilter
import org.apache.accumulo.core.iterators.user.TimestampFilter
import org.apache.accumulo.core.security.Authorizations

@SuppressWarnings("GroovyAssignabilityCheck")
class BatchScannerBuilderTest extends GroovyTestCase {
    def params = [
            instanceName: "instance",
            zookeeper: ["a", "b", "c"],
            username: "test",
            password: "password",
            useMock: true,
            tablePrefix: "prefix",
    ]
    AccumuloConnection connection

    void setUp() {
        connection = new MockAccumuloConnection(params)
    }

    void testSimpleBuild() {
        BatchScannerBuilder sb = new BatchScannerBuilder(connection)
        boolean scannedCalled = false
        def MockBatchScanner scanner =
            sb(tableName: "simpleBuild", authorizations: ["a", "b", "c"], queryThreads: 7) {
                fetch(columnFamily: "family", columnQualifier: "qual")
                ranges {
                    range(start: "a", stop: "b")
                    range(start: "c", stop: "d")
                }
            }({
                scannedCalled = true
                it
            })
        assert scannedCalled
        assert scanner.m_close_called
        def ranges = scanner.m_ranges
        def actual = ranges.collect {
            Range r -> [start: keyToRowId(r.startKey), end: keyToRowId(r.endKey)]
        }
        def expected = [[start: "a", end: "b"], [start: "c", end: "d"]]
        assert actual == expected
        assert columnToMap(scanner.fetchedColumns) == [[cf: "family", cq: "qual"]]
        assert scanner.m_tableName == "prefix_simpleBuild"
        assert asStringList(scanner.m_auths) == ["a", "b", "c"]
        assert scanner.m_queryThreads == 7
    }

    void testSimpleBuildWithRegexes() {
        BatchScannerBuilder sb = new BatchScannerBuilder(connection)
        boolean scannedCalled = false
        def MockBatchScanner scanner =
            sb(tableName: "simpleBuild", authorizations: ["a", "b", "c"], queryThreads: 7) {
                fetch(columnFamily: "family", columnQualifier: "qual")
                ranges {
                    range(start: "a", stop: "b")
                    range(start: "c", stop: "d")
                }
                columnFamilyRegex("familyRegex")
                columnQualifierRegex("qualRegex")
                rowRegex("rowRegex")
                valueRegex("myValueRegex")
            }({
                scannedCalled = true
                it
            })
        assert scannedCalled
        assert scanner.m_close_called
        def ranges = scanner.m_ranges
        def actual = ranges.collect {
            Range r -> [start: keyToRowId(r.startKey), end: keyToRowId(r.endKey)]
        }
        def expected = [[start: "a", end: "b"], [start: "c", end: "d"]]
        assert actual == expected
        assert columnToMap(scanner.fetchedColumns) == [[cf: "family", cq: "qual"]]
        assert scanner.m_tableName == "prefix_simpleBuild"
        assert asStringList(scanner.m_auths) == ["a", "b", "c"]
        assert scanner.m_queryThreads == 7

        assert scanner.serverSideIteratorOptions['regExAuto'] == [colqRegex: 'qualRegex',
                rowRegex: 'rowRegex', colfRegex: 'familyRegex', valueRegex: 'myValueRegex']
    }

    void testWithScannerOptionsFirst() {
        BatchScannerBuilder sb = new BatchScannerBuilder(connection)
        boolean scannedCalled = false
        def MockBatchScanner scanner =
            sb(tableName: "simpleBuild", authorizations: ["a", "b", "c"], queryThreads: 7) {
                scanIterator(priority: 15, iteratorClass: TimestampFilter.class) {
                    start = 1359846899
                    startInclusive()
                    end = 1359847100
                }
                ranges {
                    range(start: "a", stop: "b")
                    range(start: "c", stop: "d")
                }
            }({
                scannedCalled = true
                it
            })
        assert scannedCalled
        assert scanner.m_close_called
        def ranges = scanner.m_ranges
        def actual = ranges.collect {
            Range r -> [start: keyToRowId(r.startKey), end: keyToRowId(r.endKey)]
        }
        def expected = [[start: "a", end: "b"], [start: "c", end: "d"]]
        assert actual == expected
        assert scanner.m_tableName == "prefix_simpleBuild"
        assert asStringList(scanner.m_auths) == ["a", "b", "c"]
        assert scanner.m_queryThreads == 7
        assert scanner.serverSideIteratorList.size() == 1
        IterInfo iterInfo = scanner.serverSideIteratorList[0]
        assert iterInfo.className == TimestampFilter.class.name
        assert iterInfo.priority == 15
        assert iterInfo.iterName == "TimestampFilter"

        assert scanner.serverSideIteratorOptions["TimestampFilter"] == [start: "1359846899", startInclusive: "1", end: "1359847100"]
    }

    void testWithScannerOptionsLast() {
        BatchScannerBuilder sb = new BatchScannerBuilder(connection)
        boolean scannedCalled = false
        def MockBatchScanner scanner =
            sb(tableName: "simpleBuild", authorizations: ["a", "b", "c"], queryThreads: 7) {
                ranges {
                    range(start: "a", stop: "b")
                    range(start: "c", stop: "d")
                }
                scanIterator(priority: 15, iteratorClass: AgeOffFilter.class) {
                    ttl = 175
                    currentTime = 1359846900
                }
            }({
                scannedCalled = true
                it
            })
        assert scannedCalled
        assert scanner.m_close_called
        def ranges = scanner.m_ranges
        def actual = ranges.collect {
            Range r -> [start: keyToRowId(r.startKey), end: keyToRowId(r.endKey)]
        }
        def expected = [[start: "a", end: "b"], [start: "c", end: "d"]]
        assert actual == expected
        assert scanner.m_tableName == "prefix_simpleBuild"
        assert asStringList(scanner.m_auths) == ["a", "b", "c"]
        assert scanner.m_queryThreads == 7
        assert scanner.serverSideIteratorList.size() == 1
        IterInfo iterInfo = scanner.serverSideIteratorList[0]
        assert iterInfo.className == AgeOffFilter.class.name
        assert iterInfo.priority == 15
        assert iterInfo.iterName == "AgeOffFilter"

        assert scanner.serverSideIteratorOptions["AgeOffFilter"] == [currentTime: "1359846900", ttl: "175"]

    }

    void testWithScannerOptionsMultiIterators() {
        BatchScannerBuilder sb = new BatchScannerBuilder(connection)
        boolean scannedCalled = false
        def MockBatchScanner scanner =
            sb(tableName: "simpleBuild", authorizations: ["a", "b", "c"], queryThreads: 7) {
                scanIterator(priority: 30, iteratorClass: AgeOffFilter.class) {
                    ttl = 175
                    currentTime = 1359846900
                }
                ranges {
                    range(start: "a", stop: "b")
                    range(start: "c", stop: "d")
                }
                scanIterator(priority: 15, iteratorClass: TimestampFilter.class) {
                    start = 1359846899
                    startInclusive()
                    end = 1359847100
                }
            }({
                scannedCalled = true
                it
            })
        assert scannedCalled
        assert scanner.m_close_called
        def ranges = scanner.m_ranges
        def actual = ranges.collect {
            Range r -> [start: keyToRowId(r.startKey), end: keyToRowId(r.endKey)]
        }
        def expected = [[start: "a", end: "b"], [start: "c", end: "d"]]
        assert actual == expected
        assert scanner.m_tableName == "prefix_simpleBuild"
        assert asStringList(scanner.m_auths) == ["a", "b", "c"]
        assert scanner.m_queryThreads == 7
        assert scanner.serverSideIteratorList.size() == 2
        IterInfo iterInfo = scanner.serverSideIteratorList[0]
        assert iterInfo.className == AgeOffFilter.class.name
        assert iterInfo.priority == 30
        assert iterInfo.iterName == "AgeOffFilter"

        assert scanner.serverSideIteratorOptions["AgeOffFilter"] == [currentTime: "1359846900", ttl: "175"]

        iterInfo = scanner.serverSideIteratorList[1]
        assert iterInfo.className == TimestampFilter.class.name
        assert iterInfo.priority == 15
        assert iterInfo.iterName == "TimestampFilter"
        assert scanner.serverSideIteratorOptions["TimestampFilter"] == [start: "1359846899", end: "1359847100", startInclusive: "1"]

    }

    def static keyToMap(Key k) {
        [rowId: k.row.toString(), cf: k.columnFamily.toString(), cq: k.columnQualifier.toString(),
                vis: k.columnVisibility.toString()]
    }

    def static keyToRowId(Key k) {
        k.row.toString()
    }

    def static columnToMap(Set col) {
        col.collect { columnToMap((Column) it) }
    }

    def static columnToMap(Column col) {
        [cf: new String(col.columnFamily), cq: new String(col.columnQualifier)]
    }

    def static asStringList(Authorizations auths) {
        auths.authorizations.collect { new String(it) }.sort()  // auths reorder but if we sort its easy to know the order
    }


}
