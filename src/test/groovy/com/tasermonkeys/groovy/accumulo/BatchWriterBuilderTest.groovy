/*
 *  Copyright 2013 James Stapleton
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.tasermonkeys.groovy.accumulo

class BatchWriterBuilderTest extends GroovyTestCase {

    def params = [
            instanceName: "instance",
            zookeeper: ["a", "b", "c"],
            username: "test",
            password: "password",
            useMock: true,
            tablePrefix: "prefix",
    ]
    AccumuloConnection connection

    void setUp() {
        connection = new AccumuloConnection(params)
    }

    public void testBatchWriter() {
        connection.withWriter("testBatchWriter") {
            addMutation("rowId") {
                put(columnFamily: "cf", columnQualifier: "cq", columnVisibility: null, value: "Some value here")
                put(columnFamily: "cf2", columnQualifier: "cq1", columnVisibility: null, value: "WOW!")
            }
            put(rowId: "anotherRowId", columnFamily: "cf2", columnQualifier: "cq1", columnVisibility: "a&b", value: "How now!")
            put(rowId: "rowIdFool", columnFamily: "cfa", columnQualifier: "cqa", columnVisibility: "a", value: "Lucky")
        }
        def scanner = connection.createScanner(tableName: "testBatchWriter", authorizations: ['a', 'b'])
        scanner.range = AccumuloUtils.asRange(start: "aa", stop: "zzzzz")
        def items = [:]
        for (def item : scanner) {
            items.get(item.key.row.toString(), []).add([item.key.columnFamily.toString(),
                    item.key.columnQualifier.toString(),
                    item.key.columnVisibility.toString(),
                    item.value.toString()])
        }
        assert items.containsKey("rowId")
        assert items["rowId"] == [['cf', 'cq', "", "Some value here"], ['cf2', 'cq1', "", "WOW!"]]
        assert items.containsKey("anotherRowId")
        assert items["anotherRowId"] == [['cf2', 'cq1', "a&b", "How now!"]]
        assert items["rowIdFool"] == [['cfa', 'cqa', "a", "Lucky"]]
    }
}
