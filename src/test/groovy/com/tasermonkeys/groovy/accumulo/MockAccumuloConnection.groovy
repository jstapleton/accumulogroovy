/*
   Copyright 2013 James Stapleton

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package com.tasermonkeys.groovy.accumulo

import org.apache.accumulo.core.client.BatchScanner
import org.apache.accumulo.core.client.Scanner
import org.apache.accumulo.core.security.Authorizations

class MockAccumuloConnection extends AccumuloConnection {
    MockAccumuloConnection(Map params) {
        super(fixParams(params))
    }

    private static Map fixParams(Map params) {
        params["useMock"] = true
        params
    }

    @Override
    protected BatchScanner makeScannerWithOptions(String tableName, Authorizations auths, int scanQueryThreads) {
        new MockBatchScanner(tableName, auths, scanQueryThreads)
    }

    @Override
    protected Scanner makeSingleScannerWithOptions(String tableName, Authorizations auths) {
        new MockScanner(tableName, auths)
    }
}
