/*
   Copyright 2013 James Stapleton

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package com.tasermonkeys.groovy.accumulo

import org.apache.accumulo.core.client.BatchScanner
import org.apache.accumulo.core.client.impl.ScannerOptions
import org.apache.accumulo.core.data.Column
import org.apache.accumulo.core.data.Key
import org.apache.accumulo.core.data.Range
import org.apache.accumulo.core.data.Value
import org.apache.accumulo.core.data.thrift.IterInfo
import org.apache.accumulo.core.security.Authorizations

class MockBatchScanner extends ScannerOptions implements BatchScanner {

    public Collection<Range> m_ranges
    public boolean m_close_called
    public String m_tableName
    public Authorizations m_auths
    public int m_queryThreads
    public Map<Key, Value> m_results = [:]

    MockBatchScanner(String tableName, Authorizations auths, int queryThreads) {
        this.m_tableName = tableName
        this.m_auths = auths
        this.m_queryThreads = queryThreads
    }

    @Override
    void setRanges(Collection<Range> ranges) {
        m_ranges = ranges
    }

    @Override
    void close() {
        m_close_called = true
    }

    public List<IterInfo> getServerSideIteratorList() {
        return super.serverSideIteratorList
    }


    public Map<String, Map<String, String>> getServerSideIteratorOptions() {
        return super.serverSideIteratorOptions
    }

    @Override
    public synchronized SortedSet<Column> getFetchedColumns() {
        return super.fetchedColumns
    }

    @SuppressWarnings(["GroovyAccessibility", "GroovyUnusedDeclaration"])
    public String getRegexIterName() {
        return super.regexIterName
    }

    @Override
    Iterator<Map.Entry<Key, Value>> iterator() {
        return m_results.iterator()
    }
}
