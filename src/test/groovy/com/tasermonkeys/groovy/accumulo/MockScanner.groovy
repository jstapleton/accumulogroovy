/*
   Copyright 2013 James Stapleton

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package com.tasermonkeys.groovy.accumulo

import org.apache.accumulo.core.client.Scanner
import org.apache.accumulo.core.client.impl.ScannerOptions
import org.apache.accumulo.core.data.Column
import org.apache.accumulo.core.data.Key
import org.apache.accumulo.core.data.Range
import org.apache.accumulo.core.data.Value
import org.apache.accumulo.core.data.thrift.IterInfo
import org.apache.accumulo.core.security.Authorizations

class MockScanner extends ScannerOptions implements Scanner {

    public Range m_range
    public String m_tableName
    public Authorizations m_auths
    public int m_timeOut
    public int m_batchSize
    public Boolean m_isolation = null
    public Map<Key, Value> m_results = [:]


    MockScanner(String tableName, Authorizations auths) {
        this.m_tableName = tableName
        this.m_auths = auths
    }

    @SuppressWarnings(["GroovyAccessibility", "GroovyUnusedDeclaration"])
    public List<IterInfo> getServerSideIteratorList() {
        return super.serverSideIteratorList
    }


    @SuppressWarnings(["GroovyAccessibility", "GroovyUnusedDeclaration"])
    public Map<String, Map<String, String>> getServerSideIteratorOptions() {
        return super.serverSideIteratorOptions
    }

    @Override
    public synchronized SortedSet<Column> getFetchedColumns() {
        return super.fetchedColumns
    }

    @SuppressWarnings(["GroovyAccessibility", "GroovyUnusedDeclaration"])
    public String getRegexIterName() {
        return super.regexIterName
    }

    @Override
    void setTimeOut(int timeOut) {
        m_timeOut = timeOut
    }

    @Override
    int getTimeOut() {
        return m_timeOut
    }

    @Override
    void setRange(Range range) {
        m_range = range
    }

    @Override
    Range getRange() {
        return range
    }

    @Override
    void setBatchSize(int size) {
        m_batchSize = size
    }

    @Override
    int getBatchSize() {
        return m_batchSize
    }

    @Override
    void enableIsolation() {
        m_isolation = true
    }

    @Override
    void disableIsolation() {
        m_isolation = false
    }

    @Override
    Iterator<Map.Entry<Key, Value>> iterator() {
        return m_results.iterator()
    }
}
