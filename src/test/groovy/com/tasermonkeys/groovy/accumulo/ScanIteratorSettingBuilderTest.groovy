/*
 *  Copyright 2013 James Stapleton
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.tasermonkeys.groovy.accumulo

import org.apache.accumulo.core.client.IteratorSetting
import org.apache.accumulo.core.iterators.user.AgeOffFilter
import org.apache.accumulo.core.iterators.user.TimestampFilter

@SuppressWarnings("GroovyAssignabilityCheck")
class ScanIteratorSettingBuilderTest extends GroovyTestCase {

    public void testIteratorSettingBuilder1() {
        IteratorSetting sisb = new ScanIteratorSettingBuilder(1, "test", AgeOffFilter.class)({
            ttl = 150
            currentTime = 1359846899
        })

        assert sisb.name == "test"
        assert sisb.priority == 1
        assert sisb.iteratorClass == AgeOffFilter.class.name
        assert sisb.options.size() == 2
        assert sisb.options.ttl == "150"
        assert sisb.options.currentTime == "1359846899"
    }

    public void testIteratorSettingBuilder2() {
        IteratorSetting sisb = new ScanIteratorSettingBuilder(784, "test", AgeOffFilter.class, ttl: 150, currentTime: 1359846899)()

        assert sisb.name == "test"
        assert sisb.priority == 784
        assert sisb.iteratorClass == AgeOffFilter.class.name
        assert sisb.options.size() == 2
        assert sisb.options.ttl == "150"
        assert sisb.options.currentTime == "1359846899"
    }

    public void testIteratorSettingBuilder3() {
        IteratorSetting sisb = new ScanIteratorSettingBuilder(priority: 16, name: "test", iteratorClass: AgeOffFilter.class, ttl: 150, currentTime: 1359846899)()

        assert sisb.name == "test"
        assert sisb.priority == 16
        assert sisb.iteratorClass == AgeOffFilter.class.name
        assert sisb.options.size() == 2
        assert sisb.options.ttl == "150"
        assert sisb.options.currentTime == "1359846899"
    }

    public void testIteratorSettingBuilder4() {
        IteratorSetting sisb = new ScanIteratorSettingBuilder(234, AgeOffFilter.class, ttl: 150, currentTime: 1359846899)()

        assert sisb.name == "AgeOffFilter"
        assert sisb.priority == 234
        assert sisb.iteratorClass == AgeOffFilter.class.name
        assert sisb.options.size() == 2
        assert sisb.options.ttl == "150"
        assert sisb.options.currentTime == "1359846899"
    }

    public void testIteratorSettingBuilder5() {
        IteratorSetting sisb = new ScanIteratorSettingBuilder(154, AgeOffFilter.class)({
            ttl = 150
            currentTime = 1359846899
        })

        assert sisb.name == "AgeOffFilter"
        assert sisb.priority == 154
        assert sisb.iteratorClass == AgeOffFilter.class.name
        assert sisb.options.size() == 2
        assert sisb.options.ttl == "150"
        assert sisb.options.currentTime == "1359846899"
    }

    public void testForBooleanOption() {
        IteratorSetting sisb = new ScanIteratorSettingBuilder(154, TimestampFilter.class)({
            start = 1359846899
            startInclusive()
            end = 1359847100
        })

        assert sisb.name == "TimestampFilter"
        assert sisb.priority == 154
        assert sisb.iteratorClass == TimestampFilter.class.name
        assert sisb.options.size() == 3
        assert sisb.options.start == "1359846899"
        assert sisb.options.startInclusive == "1"
        assert sisb.options.end == "1359847100"
    }

    public void testIteratorSettingBuilder6() {
        IteratorSetting sisb = new ScanIteratorSettingBuilder(11, "test", AgeOffFilter.class.name)({
            ttl = 150
            currentTime = 1359846899
        })

        assert sisb.name == "test"
        assert sisb.priority == 11
        assert sisb.iteratorClass == AgeOffFilter.class.name
        assert sisb.options.size() == 2
        assert sisb.options.ttl == "150"
        assert sisb.options.currentTime == "1359846899"
    }

    public void testIteratorSettingBuilder7() {
        // named arguments can go anywhere, however, positional arguments must be in the proper order
        IteratorSetting sisb = new ScanIteratorSettingBuilder(ttl: 150, currentTime: 1359846899, 915, "test", AgeOffFilter.class.name)()

        assert sisb.name == "test"
        assert sisb.priority == 915
        assert sisb.iteratorClass == AgeOffFilter.class.name
        assert sisb.options.size() == 2
        assert sisb.options.ttl == "150"
        assert sisb.options.currentTime == "1359846899"
    }
}
