/*
   Copyright 2013 James Stapleton

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package com.tasermonkeys.groovy.accumulo

import org.apache.accumulo.core.data.Column
import org.apache.accumulo.core.data.Key
import org.apache.accumulo.core.data.thrift.IterInfo
import org.apache.accumulo.core.iterators.user.AgeOffFilter
import org.apache.accumulo.core.iterators.user.TimestampFilter
import org.apache.accumulo.core.security.Authorizations

@SuppressWarnings("GroovyAssignabilityCheck")
class ScannerBuilderTest extends GroovyTestCase {
    def params = [
            instanceName: "instance",
            zookeeper: ["a", "b", "c"],
            username: "test",
            password: "password",
            useMock: true,
            tablePrefix: "prefix",
    ]
    AccumuloConnection connection

    void setUp() {
        connection = new MockAccumuloConnection(params)
    }

    void testSimpleBuild() {
        ScannerBuilder sb = new ScannerBuilder(connection)
        boolean scannedCalled = false
        def MockScanner scanner =
            sb(tableName: "scannerSimpleBuild", authorizations: ["a", "b", "c"]) {
                fetch(columnFamily: "family", columnQualifier: "qual")
                range(start: "a", stop: "b")
            }({
                scannedCalled = true
                it
            })
        assert scannedCalled
        def range = scanner.m_range
        def actual = [start: keyToRowId(range.startKey), end: keyToRowId(range.endKey)]
        def expected = [start: "a", end: "b"]
        assert actual == expected
        assert columnToMap(scanner.fetchedColumns) == [[cf: "family", cq: "qual"]]
        assert scanner.m_tableName == "prefix_scannerSimpleBuild"
        assert asStringList(scanner.m_auths) == ["a", "b", "c"]
    }

    void testSimpleBuildWithRegexes() {
        ScannerBuilder sb = new ScannerBuilder(connection)
        boolean scannedCalled = false
        def MockScanner scanner =
            sb(tableName: "scannerSimpleBuild", authorizations: ["a", "b", "c"]) {
                fetch(columnFamily: "family", columnQualifier: "qual")
                range(start: "a", stop: "b")
                columnFamilyRegex("familyRegex")
                columnQualifierRegex("qualRegex")
                rowRegex("rowRegex")
                valueRegex("myValueRegex")
            }({
                scannedCalled = true
                it
            })
        assert scannedCalled
        def range = scanner.m_range
        def actual = [start: keyToRowId(range.startKey), end: keyToRowId(range.endKey)]
        def expected = [start: "a", end: "b"]
        assert actual == expected
        assert columnToMap(scanner.fetchedColumns) == [[cf: "family", cq: "qual"]]
        assert scanner.m_tableName == "prefix_scannerSimpleBuild"
        assert asStringList(scanner.m_auths) == ["a", "b", "c"]

        assert scanner.serverSideIteratorOptions['regExAuto'] == [colqRegex: 'qualRegex',
                rowRegex: 'rowRegex', colfRegex: 'familyRegex', valueRegex: 'myValueRegex']
    }

    void testWithScannerOptionsFirst() {
        ScannerBuilder sb = new ScannerBuilder(connection)
        boolean scannedCalled = false
        def MockScanner scanner =
            sb(tableName: "scannerSimpleBuild", authorizations: ["a", "b", "c"]) {
                scanIterator(priority: 15, iteratorClass: TimestampFilter.class) {
                    start = 1359846899
                    startInclusive()
                    end = 1359847100
                }
                range(start: "a", stop: "b")
            }({
                scannedCalled = true
                it
            })
        assert scannedCalled
        def range = scanner.m_range
        def actual = [start: keyToRowId(range.startKey), end: keyToRowId(range.endKey)]
        def expected = [start: "a", end: "b"]
        assert actual == expected
        assert scanner.m_tableName == "prefix_scannerSimpleBuild"
        assert asStringList(scanner.m_auths) == ["a", "b", "c"]
        assert scanner.serverSideIteratorList.size() == 1
        IterInfo iterInfo = scanner.serverSideIteratorList[0]
        assert iterInfo.className == TimestampFilter.class.name
        assert iterInfo.priority == 15
        assert iterInfo.iterName == "TimestampFilter"

        assert scanner.serverSideIteratorOptions["TimestampFilter"] == [start: "1359846899", startInclusive: "1", end: "1359847100"]
    }

    void testWithScannerOptionsLast() {
        ScannerBuilder sb = new ScannerBuilder(connection)
        boolean scannedCalled = false
        def MockScanner scanner =
            sb(tableName: "scannerSimpleBuild", authorizations: ["a", "b", "c"]) {
                range(start: "a", stop: "b")
                scanIterator(priority: 15, iteratorClass: AgeOffFilter.class) {
                    ttl = 175
                    currentTime = 1359846900
                }
            }({
                scannedCalled = true
                it
            })
        assert scannedCalled
        def range = scanner.m_range
        def actual = [start: keyToRowId(range.startKey), end: keyToRowId(range.endKey)]
        def expected = [start: "a", end: "b"]
        assert actual == expected
        assert scanner.m_tableName == "prefix_scannerSimpleBuild"
        assert asStringList(scanner.m_auths) == ["a", "b", "c"]
        assert scanner.serverSideIteratorList.size() == 1
        IterInfo iterInfo = scanner.serverSideIteratorList[0]
        assert iterInfo.className == AgeOffFilter.class.name
        assert iterInfo.priority == 15
        assert iterInfo.iterName == "AgeOffFilter"

        assert scanner.serverSideIteratorOptions["AgeOffFilter"] == [currentTime: "1359846900", ttl: "175"]

    }

    void testWithScannerOptionsMultiIterators() {
        ScannerBuilder sb = new ScannerBuilder(connection)
        boolean scannedCalled = false
        def MockScanner scanner =
            sb(tableName: "scannerSimpleBuild", authorizations: ["a", "b", "c"]) {
                scanIterator(priority: 30, iteratorClass: AgeOffFilter.class) {
                    ttl = 175
                    currentTime = 1359846900
                }
                range(start: "a", stop: "b")
                scanIterator(priority: 15, iteratorClass: TimestampFilter.class) {
                    start = 1359846899
                    startInclusive()
                    end = 1359847100
                }
            }({
                scannedCalled = true
                it
            })
        assert scannedCalled
        def range = scanner.m_range
        def actual = [start: keyToRowId(range.startKey), end: keyToRowId(range.endKey)]
        def expected = [start: "a", end: "b"]
        assert actual == expected
        assert scanner.m_tableName == "prefix_scannerSimpleBuild"
        assert asStringList(scanner.m_auths) == ["a", "b", "c"]
        assert scanner.serverSideIteratorList.size() == 2
        IterInfo iterInfo = scanner.serverSideIteratorList[0]
        assert iterInfo.className == AgeOffFilter.class.name
        assert iterInfo.priority == 30
        assert iterInfo.iterName == "AgeOffFilter"

        assert scanner.serverSideIteratorOptions["AgeOffFilter"] == [currentTime: "1359846900", ttl: "175"]

        iterInfo = scanner.serverSideIteratorList[1]
        assert iterInfo.className == TimestampFilter.class.name
        assert iterInfo.priority == 15
        assert iterInfo.iterName == "TimestampFilter"
        assert scanner.serverSideIteratorOptions["TimestampFilter"] == [start: "1359846899", end: "1359847100", startInclusive: "1"]

    }

    def static keyToMap(Key k) {
        [rowId: k.row.toString(), cf: k.columnFamily.toString(), cq: k.columnQualifier.toString(),
                vis: k.columnVisibility.toString()]
    }

    def static keyToRowId(Key k) {
        k.row.toString()
    }

    def static columnToMap(Set col) {
        col.collect { columnToMap((Column) it) }
    }

    def static columnToMap(Column col) {
        [cf: new String(col.columnFamily), cq: new String(col.columnQualifier)]
    }

    def static asStringList(Authorizations auths) {
        auths.authorizations.collect { new String(it) }.sort()  // auths reorder but if we sort its easy to know the order
    }


}
